export default ({store, app}, inject) => {
  inject('pushMessage', text => store.commit('messages/add', { text }))
}
