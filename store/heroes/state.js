export default () => ({
  heroes: [
    {id: 1, name: 'spiderman'},
    {id: 2, name: 'superman'},
    {id: 3, name: 'batman'},
    {id: 4, name: 'ironman'},
    {id: 5, name: 'captian-america'},
  ]
})
