export default {
  add (state, { name }) {
    let lastHero = _.maxBy(state.heroes, function (hero) { return hero.id; });
    const id = _.isUndefined(lastHero) ? 0 : lastHero.id
    const newHero = { id: id + 1, name: name }
    state.heroes.push(newHero)
    this.$pushMessage('added hero ' + newHero.name + ' id=' + newHero.id)
  },

  delete (state, { index }) {
    const hero = state.heroes[index]
    state.heroes.splice(index, 1)
    this.$pushMessage('deleted hero id=' + hero.id)
  },

  update (state, { hero }) {
    const index = _.findIndex(state.heroes, (h) => { return h.id == hero.id})
    if (index < 0) return
    state.heroes[index] = hero
    this.$pushMessage('updated hero id=' + hero.id)
  },
}
