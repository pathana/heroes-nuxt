export default {
  heroes (state) {
    return state.heroes
  },

  sliceHeroes (state) {
    return state.heroes.slice(0, 4)
  },

  hero: (state) => (id) => {
    return state.heroes.find(hero => hero.id == id)
  },

  filterHero: (state) => (text) => {
    return state.heroes.filter(hero => hero.name.includes(text))
  }
}
