export default {
  add (state, { text }) {
    state.messages.push(text)
  },

  clear (state) {
    state.messages = []
  }
}
